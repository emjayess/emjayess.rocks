## @emjayess personal site
"emjayess rocks"
.. site, rants, portfolio, and side hustle index

---

Latest incarnation built with astro.build
╭─────╮  Houston:
│ ◠ ◡ ◠  Good luck out there, astronaut! 🚀
╰─────╯

Canonical source repo on GitLab (`emjayess/emjayess.rocks`)
Mirrored source repo to GitHub (for visibility)

Deployed on Vercel..
`git push` and watch
